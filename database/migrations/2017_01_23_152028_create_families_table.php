<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
<<<<<<< HEAD
        Schema::create('families', function(Blueprint $table){

            $table->increments('id');
            $table->string('code', 4 )->unique();
            $table->string('name', 30);
=======
        Schema::create('families', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 4)->unique();
            $table->string('name', 40);
>>>>>>> clase
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
