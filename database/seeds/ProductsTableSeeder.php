<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'code' => 'RA01',
            'name' => 'Ratón inalámbrio',
            'price' => 10,
            'family_id' => 1,
        ]);
        
        DB::table('products')->insert([
            'code' => 'RA02',
            'name' => 'Ratón multimedia',
            'price' => 12,
            'family_id' => 1,
        ]);
    }
}
