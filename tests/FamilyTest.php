<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FamilyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;



    public function testLista()
    {
        $this->visit('/families')
             ->see('Lista de familias')
<<<<<<< HEAD
             ->see('MICR');
=======
             ->see('MICR')
             ->see('»');
             ;
>>>>>>> clase
    }

    public function testCreate()
    {
        //verificar validaciones
        //verificar campos obligatorios
        $this->visit('/families')
             ->see('Nuevo')
             ->click('Nuevo')
             ->seePageIs('/families/create')
             ->see('Guardar')
             ->press('Guardar')
             ->see('El campo código es obligatorio')
             ->see('El campo nombre es obligatorio')
             ;
        //verificar longitud de campos
        $this->visit('/families')
             ->see('Nuevo')
             ->click('Nuevo')
             ->seePageIs('/families/create')
             ->type('ABCDE', 'code')
             ->type('01234567890123456789012345678901234567891', 'name')
             ->see('Guardar')
             ->press('Guardar')
             ->see('código no debe ser mayor que 4 caracteres')
             ->see('nombre no debe ser mayor que 40 caracteres')
             ;
        //verificar duplicidad de código
        $this->visit('/families')
             ->see('Nuevo')
             ->click('Nuevo')
             ->seePageIs('/families/create')
             ->type('MICR', 'code')
             ->type('xxxx', 'name')
             ->see('Guardar')
             ->press('Guardar')
             ->see('código ya ha sido registrado')
             ;

        //Grabar datos
        $this->visit('/families')
             ->see('Nuevo')
             ->click('Nuevo')
             ->seePageIs('/families/create')
             ->type('WXYZ', 'code')
             ->type('Familia WXYZ', 'name')
             ->see('Guardar')
             ->press('Guardar')
             ->seePageIs('/families')
<<<<<<< HEAD
             ->seeInDatabase('families', [
                'code' => 'WXYZ', 
                'name' => 'Familia WXYZ'])
=======
             ->see('»')
             ->click('»')
             ->see('WXYZ')
             ->see('Familia WXYZ')
>>>>>>> clase
             ;
    }

    public function testEdit()
    {
        //Editar datos
        $this->visit('/families')
             ->see('Editar')
             ->click('Editar')
             ->seePageIs('/families/1/edit')
             ->type('micr', 'code')
             ->type('Familia micros', 'name')
             ->see('Guardar')
             ->press('Guardar')
             ->seePageIs('/families')
             ->visit('/families/1')
<<<<<<< HEAD
             ->seeInDatabase('micr')
             ->seeInDatabase('Familia micros')
=======
             ->see('micr')
             ->see('Familia micros')
>>>>>>> clase
             ;

    }

    public function testJson()
    {
        //probar con JSON
        $this->put('/families/1', ['code' => 'MCR', 'name' => 'Microprocesadores'])
        // ->seeJson(['updated' => true])
        // ->dump()
         ;

    }
<<<<<<< HEAD
}
=======
}
>>>>>>> clase
