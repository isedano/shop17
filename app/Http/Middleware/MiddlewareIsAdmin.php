<?php

namespace App\Http\Middleware;

use Closure;

class MiddlewareIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        //dd($user);          -> Mostrar datos de user
        //dd($user->name);    ->Mostrar nombre de user
        if($user && $user->isAdmin()) {  //si existe un usuario y si se llama admin se mete 
            return $next($request);
        } 
        abort(404,"Por ahi no se puede"); // si no es el administrador, se lanza un mensaje de error
    }
    
 }
