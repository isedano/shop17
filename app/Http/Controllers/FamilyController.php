<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;
use Illuminate\Validation\Rule;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
<<<<<<< HEAD
    public function index(Request $request)
    {
        //$families = Family::all();
        $families = Family::paginate(4);

=======
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        // $families = Family::all();
        $families = Family::paginate(config('param.pageSize'));
>>>>>>> clase
        if ($request->ajax()) {
            return $families;
        } else {
            return view('family.index', [
                'families' => $families]);
        }
    }

    public function ajax()
    {
        return view('family.ajax');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
<<<<<<< HEAD
        'code' => 'required|max:4|unique:families',
=======
        'code' => 'required|unique:families|max:4',
>>>>>>> clase
        'name' => 'required|max:40',
        ]);

        $family = new Family($request->all());
        $family->save();
        if ($request->ajax()) {
            return response()->json([true]);
        } else {
            return redirect('/families');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

//        $family = Family::find(1);
        $family = Family::findOrFail($id);
        if ($request->ajax()) {
            return $family;
        } else {
            return view('family.show', ['family' => $family]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $family = Family::findOrFail($id);
        return view('family.edit', ['family' => $family]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
<<<<<<< HEAD
        //unique:table,column,exception, idColumn
        $this->validate($request, [
                'code' => 'required|max:4|unique:families,id,'. $id,
=======
        $this->validate($request, [
        'code' => 'required|max:4|unique:families,id,' . $id,
>>>>>>> clase
        'name' => 'required|max:40',
        ]);

        $family = Family::findOrFail($id);
        $family->code = $request->code;
        $family->name = $request->name;
        $family->save();
        if ($request->ajax()) {
            return response()->json(['updated' => true]);
        } else {
            return redirect('/families');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        Family::find($id)->delete();
        //Family::destroy($id);
        if ($request->ajax()) {
            return response()->json([true]);
        } else {
            return redirect('/families');
        }
    }
<<<<<<< HEAD
}
=======
}
>>>>>>> clase
